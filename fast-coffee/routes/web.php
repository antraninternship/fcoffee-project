<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
App::register('Gloudemans\Shoppingcart\ShoppingcartServiceProvider');

Route::get('/', 'UserController@index');
Route::get('/login','SessionController@create');
Route::get('/register','RegisterController@create');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::prefix('admin')->group(function(){
    Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login','Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
});

Route::resource('shops','ShopController');
Route::resource('menus','MenuController');
Route::resource('cart','CartController');
Route::resource('ship','ShipController');
Route::resource('search','SearchController');
Route::get('/search','SearchController@create');
Route::get('/menus/show/{id}','MenuController@show');
Route::get('/cart/{id}','CartController@create');
