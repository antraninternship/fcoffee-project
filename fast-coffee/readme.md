# README #

* The "Dn Coffee" is a website sell coffee online. When you navigate to our website. You can see products special or coffee shops famous in Danang. This website    
  has 2 searches bar, Search base on Name shop or Name drinks and foods. With search base on name shop, After searching you will see all products in this shop.    
  And with search base on Name drinks or foods, you will see this product you seached with a lot of shop has this ptoduct.
* Each product has a plus button. you click on this button to add to order table. After adding product. You go to next step is order. To order you have to fill    
  fully information and click on order button to finish order.

### What is this repository for "Dn Coffee" website? ###

* Quick summary
* Version
* git@bitbucket.org:antraninternship/fcoffee-project.git

### How do I get set up? ###
* Check  your server meets the following requirements:
    * PHP >= 5.6.4
    * OpenSSL PHP Extension
    * PDO PHP Extension
    * Mbstring PHP Extension
    * Tokenizer PHP Extension
    * XML PHP Extension
    ---------------------------------------------------------------------
    Check php version:
        php --version
    check PHP Extension: Verify you have openssl, PDO, mbstring, tokenizez, xml
        php -m
    If your serve empty one in all or empty some this one:
        sudo apt-get install php mbstring: If empty mbstring
        sudo apt-get install php xml: If empty xml
    There are two requires empty in ubuntu. In window, you no need add "sudo" and almost requires have done
    ---------------------------------------------------------------------

* Install composer:
    ---------------------------------------------------------------------
    * Ubuntu 16.04 LTS https://getcomposer.org/download/
       * php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
       * php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
       * php composer-setup.php
       * php -r "unlink('composer-setup.php');"

    * Window 10 https://getcomposer.org/download/
       * php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
       * php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
       * php composer-setup.php
       * php -r "unlink('composer-setup.php');"
    * in window, You can click "Composer-Setup.exe" to dowload and run composer without using command line
    ---------------------------------------------------------------------

* Install apche 2/xampp
    ---------------------------------------------------------------------
    * Ubuntu 16.04 LTS
        sudo apt-get install apche 2

    * Window 10 https://www.apachefriends.org/download.html
        Click to link and choose xampp match with your serve
    ---------------------------------------------------------------------

* Install Laravel 5.4
    ---------------------------------------------------------------------
    * Ubuntu 16.04 LTS https://laravel.com/docs/5.4/installation
        php composer.phar global require "laravel/installer"

    * Window 10 https://laravel.com/docs/5.4/installation
        composer global require "laravel/installer"    
    ---------------------------------------------------------------------

* Clone project
    ---------------------------------------------------------------------
        HTTPS: https://TranAnIT@bitbucket.org/antraninternship/fcoffee-project.git
        SSH: git@bitbucket.org:antraninternship/fcoffee-project.git
    * Depend your serve you can choose HTTPS or SSH. You should choose HTTPS if your serve did not set upp key.

        * First you have to clone source code
        git clone https://TranAnIT@bitbucket.org/antraninternship/fcoffee-project.git
        * Cd to project: open command line
            cd ~/fcoffee-project/fast-coffee
        * Pull develop
            git checkout develop
            git pull origin develop
    ---------------------------------------------------------------------

* Check node version and npm vesion    

    ---------------------------------------------------------------------
    * Check node, npm and composer version
        node -v or node --version
        npm -v or npm --version
        composer -v or composer --version

    * If your project did not install
        node install
        npm install
        composer install
    ---------------------------------------------------------------------

* Run project

    ---------------------------------------------------------------------
    * Set up your serve
       * Cd to your project
       * Copy the content in .env.example file then create a new file with the name is .env
       * Config database name in .env file
            DB_CONNECTION=mysql
            DB_HOST=127.0.0.1DB_PORT=3306
            DB_DATABASE=fast_coffee
            DB_USERNAME=root
            DB_PASSWORD=

* In window: You have to open xampp and start Apache and MySQL before navigating to localhost/phpmyadmin
* Go to http://localhost/phpmyadmin/ and create name database "fast-coffee"
    * Ubuntu 16.04 LTS: You have to enter username and password. When you install apache, you have to enter username and password and set up it in .env file
* Generate the key of project by using command line: php artisan key:generate
* Run command: "php artisan migrate" to migrate the table into database
* On project folder. You have to type "npm run dev" to build all files on resources folder into public folder that makes the project can work correctly.
* Direct to correct folder of project and type command line: "php artisan serve" to run
* Get link and run your local.
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

