<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart as Cart;

class UserController extends Controller
{
  public function index() {
		$cartItems = Cart::content();

		return view('home', compact('cartItems'));
  }
}
