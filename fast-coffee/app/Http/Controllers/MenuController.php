<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Menu;
use App\Shop;
use Session;
use Intervention\Image\ImageManagerStatic as Image;

class MenuController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$menus = Menu::all();
		return view('menus.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $shops = Shop::all();
		return view('menus.create')->withShops($shops);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,array(
			'name' => 'required|Max:255',
			'price' => 'required',
			'status' => 'required|Max:15',
			'description' => 'required'
		));

		// store in the database
		$menu = new Menu;

		$menu->name = $request->name;
		$menu->shop_id = $request->shop_id;
		$menu->price = $request->price;
		$menu->status = $request->status;
		$menu->description = $request->description;


		if ($request->hasFile('image')) {
			$image = $request->file('image');
			$filename = time() . '.' . $image->getClientOriginalExtension();
			$location = public_path('images/' . $filename);

			Image::make($image)->resize(800, 400)->save($location);

		}

		$menu->image = 'images/' . $filename;
		$menu->save();
		return redirect()->route('menus.create');
		 Session::flash('successful', 'The product was successfully save!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$menus = Menu::find($id);
        return \View::make('menus.show', compact('menus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
