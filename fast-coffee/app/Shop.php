<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $table = 'shops';

	public function menus() {
		return $this->hasMany('App\Menu');
	}
}
