<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menus';
	public function shop() {
		return $this->belongsTo('App\Shop');
	}
}
