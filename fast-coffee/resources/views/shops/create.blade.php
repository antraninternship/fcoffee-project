@extends('layouts.app')

@section('Name','|Add shop')
  {!! Html::style('css/parsley.css') !!}
@section('content')
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <h2>Create new shop</h2>
      <hr>
      {!! Form::open(array('route' => 'shops.store', 'data-parsley-validate' => '', 'files' => true)) !!}
        {{ Form::label('name','Name shop:') }}
        {{ Form::text('name',null,array('class'=>'form-control', 'required' => '')) }}

        {{ Form::label('address','Address shop:') }}
        {{ Form::text('address',null,array('class'=>'form-control', 'required' => '')) }}

        {{ Form::label('phone','Phone number:') }}
        {{ Form::text('phone',null,array('class'=>'form-control', 'required' => '', 'maxlength' => '15')) }}

        {{ Form::label('email','Email:') }}
        {{ Form::email('email',null,array('class'=>'form-control', 'required' => '')) }}

        {{ Form::label('image','Upload image:') }}
        {{ Form::file('image') }}

        {{ Form::label('description','Description:') }}
        {{ Form::textarea('description',null,array('class'=>'form-control', 'required' => '')) }}

        {{Form::submit('Add shop',array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top:20px'))}}

      {!! Form::close() !!}
    </div>
  </div>
@endsection
{!! Html::script('js/parsley.min.js') !!}

