@extends('layouts.app')
@section('content')
<div class="container">
  @if (sizeof(Cart::content()) > 0)
    <table class="table">
      <thead>
        <tr>
          <th>ID</th>
          <th>Coffee name</th>
          <th>Quantity</th>
          <th>Price</th>
          <th>Action</th>
          <th class="column-spacer"></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach (Cart::content() as $item)
        <tr>
          <td>{{ $item->id }}</td>
          <td>{{ $item->name }}</td>
          <td width="50px">
            {!! Form::open(['route' => ['cart.update',$item->rowId], 'method' => 'PUT']) !!}
            <input name="qty" type="text" value="{{$item->qty}}">
          </td>
          <td>${{ $item->subtotal }}</td>
          <td>
            <form action="{{ url('cart', [$item->rowId]) }}" method="POST" class="side-by-side">
              {!! csrf_field() !!}
              <input type="hidden" name="_method" value="DELETE">
              <input type="submit" class="btn btn-danger btn-sm" value="Remove">
            </form>
          </td>
        </tr>
        @endforeach
        <tr>
          <td></td>
          <td class="small-caps table-bg" style="text-align: right">Subtotal</td>
          <td>${{ Cart::instance('default')->subtotal() }}</td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td class="small-caps table-bg" style="text-align: right">Tax</td>
          <td>${{ Cart::instance('default')->tax() }}</td>
          <td></td>
          <td></td>
        </tr>

        <tr class="border-bottom">
          <td style="padding: 40px;"></td>
          <td class="small-caps table-bg" style="text-align: right">Your Total</td>
          <td class="table-bg">${{ Cart::total() }}</td>
          <td class="column-spacer"></td>
          <td></td>
        </tr>

      </tbody>
    </table>

    <a href="{{ url('/') }}" class="btn btn-primary btn-lg">Continue Shopping</a> &nbsp;
    <a href="{{ route('cart.create',[$item->rowId]) }}" class="btn btn-success btn-lg">Proceed to Checkout</a>

  @else

    <h3>You have no items in your shopping cart</h3>
    <a href="{{ url('/') }}" class="btn btn-primary btn-lg">Continue Shopping</a>

  @endif

  <div class="spacer"></div>

</div> <!-- end container -->
@endsection
