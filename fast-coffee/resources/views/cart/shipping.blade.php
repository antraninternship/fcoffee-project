@extends('layouts.app')

{!! Html::style('css/parsley.css') !!}
@section('content')

  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      @if(Session::has('message'))
        <div class="alert alert-info">
          {{ Session::get('message') }}
        </div>
      @endif
      <h1>Order Information</h1>
      <hr>
      {!! Form::open(array('route' => 'ship.store', 'data-parsley-validate' => '', 'files' => true, 'method' =>'post')) !!}
        {{ Form::label('name','Full name:') }}
        {{ Form::text('name',null,array('class'=>'form-control', 'required' => '')) }}

        {{ Form::label('phone','Phone number:') }}
        {{ Form::text('phone',null,array('class'=>'form-control', 'required' => '', 'maxlength' => '15')) }}

        {{ Form::label('address','Address:') }}
        {{ Form::text('address',null,array('class'=>'form-control', 'required' => '')) }}

        {{ Form::label('date','Choose date & time:') }}
        <input type="datetime-local" name="date" class="form-control" id="datepicker">

        {{Form::submit('Order',array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top:20px'))}}

      {!! Form::close() !!}
    </div>
  </div>

@endsection
{!! Html::script('js/parsley.min.js') !!}

