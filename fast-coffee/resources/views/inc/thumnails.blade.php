<section class="row products">
  <div class="col-xs-6 col-md-3">
    <div class="thumbnail">
      <a href="#">
        <img src="/images/capuchino.png" alt="...">
      </a>
      <div class="caption">
        <a href="#">
          <p>Capuchino</p>
          <p>125,000 Vnd</p>
        </a>
        <a href="#">
          <button type="submit" class="btn btn-danger"><strong>+</strong></button>
        </a>
      </div>
    </div>
  </div>

  <div class="col-xs-6 col-md-3">
    <div class="thumbnail">
      <a href="#">
        <img src="/images/coffee-egg.jpeg" alt="...">
      </a>
      <div class="caption">
        <a href="#">
          <p>Capuchino</p>
          <p>125,000 Vnd</p>
        </a>
        <a href="#">
          <button type="submit" class="btn btn-danger"><strong>+</strong></button>
        </a>
      </div>
    </div>
  </div>

  <div class="col-xs-6 col-md-3">
    <div class="thumbnail">
      <a href="#">
        <img src="/images/cf-tall.jpg" alt="...">
      </a>
      <div class="caption">
        <a href="#">
          <p>Capuchino</p>
          <p>125,000 Vnd</p>
        </a>
        <a href="#">
          <button type="submit" class="btn btn-danger"><strong>+</strong></button>
        </a>
      </div>
    </div>
  </div>

  <div class="col-xs-6 col-md-3">
    <div class="thumbnail">
      <a href="#">
        <img src="/images/cotta.jpg" alt="...">
      </a>
      <div class="caption">
        <a href="#">
          <p>Capuchino</p>
          <p>125,000 Vnd</p>
        </a>
        <a href="#">
          <button type="submit" class="btn btn-danger"><strong>+</strong></button>
        </a>
      </div>
    </div>
  </div>

  <div class="col-xs-6 col-md-3">
    <div class="thumbnail">
      <a href="#">
        <img src="/images/coffee-cotta.jpg" alt="...">
      </a>
    <div class="caption">
      <a href="#">
        <p>Capuchino</p>
        <p>125,000 Vnd</p>
      </a>
      <a href="#">
        <button type="submit" class="btn btn-danger"><strong>+</strong></button>
      </a>
    </div>
  </div>
</div>

  <div class="col-xs-6 col-md-3">
    <div class="thumbnail">
      <a href="#">
        <img src="/images/cotta-love.jpg" alt="...">
      </a>
      <div class="caption">
        <a href="#">
          <p>Capuchino</p>
          <p>125,000 Vnd</p>
        </a>
        <a href="#">
          <button type="submit" class="btn btn-danger"><strong>+</strong></button>
        </a>
      </div>
    </div>
  </div>

  <div class="col-xs-6 col-md-3">
    <div class="thumbnail">
      <a href="#">
        <img src="/images/cota-cf.png" alt="...">
      </a>
      <div class="caption">
        <a href="#">
          <p>Capuchino</p>
          <p>125,000 Vnd</p>
        </a>
        <a href="#">
          <button type="submit" class="btn btn-danger"><strong>+</strong></button>
        </a>
      </div>
    </div>
  </div>

  <div class="col-xs-6 col-md-3">
    <div class="thumbnail">
      <a href="#">
        <img src="/images/mocha.jpg" alt="...">
      </a>
      <div class="caption">
        <a href="#">
          <p>Capuchino</p>
          <p>125,000 Vnd</p>
        </a>
        <a href="#">
          <button type="submit" class="btn btn-danger"><strong>+</strong></button>
        </a>
      </div>
    </div>
  </div>
</section>