<div class="flipkart-navbar navbar-fix">
  <div class="container">
    <div class="row row1">
      <ul class="pull-right">
        <li class="upper-links active"><a class="links" href="/">Home</a></li>
        <li class="upper-links"><a class="links" href="#">Shop</a></li>
        <li class="upper-links"><a class="links" href="#">Blog</a></li>
        <li class="upper-links"><a class="links" href="#">About</a></li>
        @if (Auth::guest())
          <li class="upper-links"><a class="links" href="{{ route('login') }}">Login</a></li>
          <li class="upper-links"><a class="links" href="{{ route('register') }}">Register</a></li>
        @else
        <li class="dropdown upper-links">
          <a href="#" class="dropdown-toggle links" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li>
              <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                Logout
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form>
            </li>
          </ul>
        </li>
        @endif
      </ul>
    </div>

    <div class="row row2">
      <div class="col-md-2"><h1 style="margin:0px;"><a class="title-header" href="/">Dn Coffee</a></h1></div>
      <div class="flipkart-navbar-search col-md-8">
        <div class="row">
          <form class="searchform cf" action="{{ route('search.create') }}">
            <input  type="text" class="flipkart-navbar-input col-md-11" name="search" placeholder="Is it me you’re looking for?">
            <button type="submit" class="flipkart-navbar-button col-md-1"><span class="glyphicon glyphicon-search"></span></button>
          </form>
        </div>
      </div>

      <div class="cart col-md-2">
        <a class="cart-button" href="{{route('cart.index')}}">
          <i class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></i> Cart
          <span class="item-number ">{{ Cart::count()}}</span>
        </a>
      </div>
    </div>
  </div>
</div>
@yield('content')

