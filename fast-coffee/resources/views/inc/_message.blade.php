@if (Session::has('successful'))

  <div class="alert alert-success" role="alert">
    <strong>Success:</strong> {{ Session::get('successful')}}
  </div>

@endif
