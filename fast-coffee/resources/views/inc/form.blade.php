<center><h2>Order Table</h2></center>
<form class="form-horizontal" action="#">
  <div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
      <div class="form-group group-form">
        <label class="control-label col-sm-2">Name:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="name" placeholder="Enter your name" name="name">
        </div>
      </div>

      <div class="form-group group-form">
        <label class="control-label col-sm-2">Phone:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="phone" placeholder="Enter your phone" name="phone">
        </div>
      </div>

      <div class="form-group group-form">
        <label class="control-label col-sm-2">Address:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="address" placeholder="Enter your address" name="addressname">
        </div>
      </div>

      <div class="form-group group-form">
        <label class="control-label col-sm-2">Date:</label>
        <div class="col-sm-10">
          <input type="date" class="form-control" id="date" name="date">
        </div>
      </div>

      <div class="form-group group-form">
        <label class="control-label col-sm-2">Time:</label>
        <div class="col-sm-10">
          <input type="time" class="form-control" id="time" name="time">
        </div>
      </div>

    </div>
    <div class="col-md-4"></div>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>No.</th>
          <th>Coffee name</th>
          <th>Quantity</th>
          <th>Price</th>
          <th>Sale</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="order-button">
    <button type="submit" class="btn btn-primary">Order</button>
  </div>
</form>
