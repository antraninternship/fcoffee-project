@extends('layouts.app')

@section('Name','|Add Product')
  {!! Html::style('css/parsley.css') !!}
@section('content')

  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <h2>Add Product</h2>
      <hr>
      {!! Form::open(array('route' => 'menus.store', 'data-parsley-validate' => '', 'files' => true)) !!}
        {{ Form::label('name','Name product:') }}
        {{ Form::text('name',null,array('class'=>'form-control', 'required' => '')) }}

        {{ Form::label('price','Price:') }}
        {{ Form::text('price',null,array('class'=>'form-control', 'required' => '')) }}

        {{ Form::label('status','Status:') }}
        {{ Form::text('status',null,array('class'=>'form-control', 'required' => '', 'maxlength' => '15')) }}

        {{ Form::label('shop_id', 'Shop:') }}
          <select name="shop_id" class="form-control">
            @foreach($shops as $shop)
              <option value='{{ $shop->id }}'>{{ $shop->name }}</option>
            @endforeach
          </select>

        {{ Form::label('image','Upload image product:') }}
        {{ Form::file('image') }}

        {{ Form::label('description','Description:') }}
        {{ Form::textarea('description',null,array('class'=>'form-control', 'required' => '')) }}

        {{Form::submit('Add product',array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top:20px'))}}

      {!! Form::close() !!}
    </div>
  </div>

@endsection
{!! Html::script('js/parsley.min.js') !!}
