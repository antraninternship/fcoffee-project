@extends('layouts.app')
@section('content')
<div class="container">
@if(isset($details))
  <section class="row products">
    @foreach($details as $menu)
    <div class="col-xs-6 col-md-3">
      <div class="thumbnail">
        <a href="{{ url('menus/show/'.$menu->id) }}">
          <img src="/{{ $menu->image }}" alt="...">
        </a>
        <div class="caption">
          <a href="{{ url('menus/show/'.$menu->id) }}">
            <p>{{ $menu->name }}</p>
            <p><em>${{ $menu->price }}</em> </p>
          </a>
          <a href="{{ route('cart.edit',$menu->id) }}">
            <button class="add-to-cart" type="submit">Add to cart</button>
          </a>
        </div>
      </div>
    </div>
    @endforeach
  </section>
  @endif
   <div class="pagination pull-right">
  {!! $details->links() !!}
  </div>
</div>
@endsection
