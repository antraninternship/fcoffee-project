<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Dn Coffee</title>
	<link rel="stylesheet" href="/css/app.css">
	<link rel="stylesheet" href="/css/custom.css">
  <link rel="stylesheet" href="/css/parsley.css">
</head>
<body>
	<header>
	  @include('inc.navbar')
 	</header>
<div class="container">
    <div class="title-products">
      <h2>{{ $menus->name }}</h2>
      <hr>
    </div>
    <div class="infor-products">
    <div class="row">
      <div class="col-md-4">
        <div class="show-image">
          <a href="#">
            <image src="/{{ $menus->image }}" class="img-thumbnail"></image>
          </a>
        </div>
      </div>
      <div class="col-md-8">
        <table class="table table-border">
          <tr>
            <td>Product name:</td>
            <td>{{ $menus->name }}</td>
          </tr>
          <tr>
            <td>Price:</td>
            <td>{{ $menus->price }}</td>
          </tr>
          <tr>
            <td>suplier:</td>
            <td>{{ $menus->shop['name'] }}</td>
          </tr>
          <tr>
            <td>Address:</td>
            <td>{{ $menus->shop['address'] }}</td>
          </tr>
        </table>
        <a href="{{ route('cart.create',[$menus->id]) }}" class="btn btn-success btn-sm">Proceed to Checkout</a>
        <a href="{{ route('cart.edit',$menus->id) }}" class="btn btn-danger btn-sm">Add to cart</a>
      </div>
    </div>
    </div>
    <hr>
    <div class="desc-products">
      <h4>Description</h4>
      {{ $menus->description }}
    </div>
</div>
 	<footer>
  	@include('inc.footer')
 	</footer>
   <script type="" src="/js/jquery.min.js"></script>
	 <script type="" src="/js/bootstrap.min.js"></script>
   <script type="" src="/js/parsley.min.js"></script>
</body>
</html>

