<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Dn Coffee</title>
	<link rel="stylesheet" href="/css/app.css">
	<link rel="stylesheet" href="/css/custom.css">
  <link rel="stylesheet" href="/css/parsley.css">
</head>
<body>
	<header>
	  @include('inc.navbar')
 	</header>
	<div class="container container-style">
    @include('inc._messages')
		@if (Request::is('/'))
			@include('inc.slide')
			@include('menus.index')
		@endif
	</div>
 	<footer>
  	@include('inc.footer')
 	</footer>
   <script type="" src="/js/jquery.min.js"></script>
	 <script type="" src="/js/bootstrap.min.js"></script>
   <script type="" src="/js/parsley.min.js"></script>
</body>
</html>
